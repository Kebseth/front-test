import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ZinderService} from '../../service/zinderService';
import {Match} from '../../model/match.model';
import {Interet} from '../../model/interet.model';

@Component({
  selector: 'app-pretender',
  templateUrl: './pretender.component.html',
  styleUrls: ['./pretender.component.css']
})
export class PretenderComponent implements OnInit {

  @Input() nom: string;
  @Input() prenom: string;
  @Input() photoUrl: string;
  @Input() interets: Array<string> = [];
  @Input() id: string;

  match: boolean;
  @Output() emitMatch: EventEmitter<Match> = new EventEmitter<Match>();

  constructor(private zinderService: ZinderService) {
  }

  ngOnInit() {

  }

  getMatch(match) {
    this.zinderService.matchPretender(this.id, match).subscribe(json => {
      this.match = json.match;
      this.emitMatch.emit(json);
    });
  }

}
