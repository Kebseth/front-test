import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PretenderComponent } from './pretender.component';

describe('PretenderComponent', () => {
  let component: PretenderComponent;
  let fixture: ComponentFixture<PretenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PretenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PretenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
