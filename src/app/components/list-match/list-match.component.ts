import { Component, OnInit } from '@angular/core';
import {Match} from '../../model/match.model';
import {ZinderService} from '../../service/zinderService';
import {Profil} from '../../model/profil.model';

@Component({
  selector: 'app-list-match',
  templateUrl: './list-match.component.html',
  styleUrls: ['./list-match.component.css']
})
export class ListMatchComponent implements OnInit {
  listMatch: Match[] = [];
  listPretenders: Profil[] = [];

  constructor(private zinderService: ZinderService) { }

  ngOnInit() {
    this.getMatchs();
    this.getPretenders();
  }

  getMatchs() {
    this.zinderService.getMatchs().subscribe(json => {
      this.listMatch = json;
    });
  }

  getPretenders() {
    this.zinderService.getPretenders().subscribe(json => {
      // @ts-ignore
      this.listPretenders = json.profils;
    });
  }
  onMatchDelete($event) {
    this.listMatch = this.listMatch.filter(match => match.id !== $event);
  }

}
