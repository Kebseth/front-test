import {Component, Input, OnInit} from '@angular/core';
import {ZinderService} from '../../service/zinderService';

@Component({
  selector: 'app-interet',
  templateUrl: './interet.component.html',
  styleUrls: ['./interet.component.css']
})
export class InteretComponent implements OnInit {

  @Input() interets: Array<string> = [];
  ListOfInterestStringigy: string[] = [];

  constructor(private zinderService: ZinderService) {
  }

  ngOnInit() {
    this.getInterest();
  }

  getInterest() {
    this.zinderService.getInterest().subscribe(json => {
      json.forEach(interestAPI => {
        this.interets.forEach(selfInterest => {
          if (interestAPI.id === selfInterest) {
            this.ListOfInterestStringigy.push(interestAPI.nom);
          }
        });
      });
    });
  }

}
