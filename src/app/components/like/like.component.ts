import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ZinderService} from '../../service/zinderService';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.css']
})
export class LikeComponent implements OnInit {
  @Input() id: string;
  @Output() emitMatch: EventEmitter<boolean> = new EventEmitter();
  match: boolean;

  constructor(private zinderService: ZinderService) {
  }

  ngOnInit() {
  }

  clickMatch(match: boolean) {
    this.emitMatch.emit(match);
  }

}
