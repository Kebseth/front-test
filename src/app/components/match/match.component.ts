import {Component, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Profil} from '../../model/profil.model';
import {ZinderService} from '../../service/zinderService';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit, OnChanges {
  @Input() id: string;
  @Input() profile: string;
  @Input() match: boolean;
  @Input() listOfPretenders;
  @Output() delete: string;
  pretend: string;

  constructor(private zinderService: ZinderService) {
  }

  ngOnInit() {
    this.convertId();
  }

  ngOnChanges() {
    this.convertId();
  }

  convertId() {
    this.listOfPretenders.forEach(people => {
      if (people.id === this.profile) {
        this.pretend = `${people.prenom} ${people.nom}`;
      }
    });
  }
}
