import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPretendersComponent } from './list-pretenders.component';

describe('ListPretendersComponent', () => {
  let component: ListPretendersComponent;
  let fixture: ComponentFixture<ListPretendersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPretendersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPretendersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
