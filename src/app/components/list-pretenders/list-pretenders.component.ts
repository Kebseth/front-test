import {Component, OnInit} from '@angular/core';
import {ZinderService} from '../../service/zinderService';
import {Profil} from '../../model/profil.model';
import {Interet} from '../../model/interet.model';

@Component({
  selector: 'app-list-pretenders',
  templateUrl: './list-pretenders.component.html',
  styleUrls: ['./list-pretenders.component.css']
})
export class ListPretendersComponent implements OnInit {

  listPretenders: Profil[] = [];
  listOfInterest: Interet[] = [];
  filter: string;

  constructor(private zinderService: ZinderService) {
  }

  ngOnInit() {
    this.getPretenders();
    this.getInterest();

  }

  getPretenders() {
    this.zinderService.getPretenders().subscribe(json => {
      // @ts-ignore
      this.listPretenders = json.profils;
    });
  }

  getInterest() {
    this.zinderService.getInterest().subscribe(json => {
      this.listOfInterest = json;
    });
  }

  filterBy(filter: string) {
    this.filter = filter;
    if (this.filter !== '') {
      this.listPretenders = this.listPretenders.filter(pretender => {
        pretender.interets.forEach(interet => {
            console.log('filter : ' + filter);
            console.log('interet : ' + interet);
            interet === filter;
          }
        );
      });
    } else {
      this.getPretenders();
    }
    console.log('3h passé dessus');
  }
}

