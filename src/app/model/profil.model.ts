export class Profil {

  nom: string;

  prenom: string;

  photoUrl: string;

  interets: Array<string>;

  id: string;

  constructor(nom: string, prenom: string, photoUrl: string, interets: Array<string>, id: string) {
    this.nom = nom;
    this.prenom = prenom;
    this.photoUrl = photoUrl;
    this.interets = interets;
    this.id = id;
  }
}
