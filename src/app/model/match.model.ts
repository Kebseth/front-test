export class Match {
  id: string;

  profil: string;

  match: boolean;

  constructor(id: string, profil: string, match: boolean) {
    this.id = id;
    this.profil = profil;
    this.match = match;
  }
}
