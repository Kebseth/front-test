import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Profil} from '../model/profil.model';
import {Match} from '../model/match.model';
import {NewMatch} from '../model/newMatch.model';
import {Interet} from '../model/interet.model';

@Injectable()
export class ZinderService {

  constructor(private http: HttpClient) {
  }

  getPretenders(): Observable<Profil[]> {
    return this.http.get<Profil[]>('http://localhost:8088/zinder/profils');
  }

  matchPretender(id: string, match: boolean): Observable<Match> {
    return this.http.post<Match>(`http://localhost:8088/zinder/profils/${id}/match`, new NewMatch(match));
  }

  getMatchs(): Observable<Match[]> {
    return this.http.get<Match[]>('http://localhost:8088/zinder/matchs');
  }

  getInterest(): Observable<Interet[]> {
    return this.http.get<Interet[]>('http://localhost:8088/zinder/interets');
  }

  deleteMatch(idMatch: string): void {
    this.http.delete(`http://localhost:8088/zinder//matchs/${idMatch}`);
  }

}
