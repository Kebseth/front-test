import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import {RouterModule, Routes} from '@angular/router';
import { PretenderComponent } from './components/pretender/pretender.component';
import {HttpClientModule} from '@angular/common/http';
import { ListPretendersComponent } from './components/list-pretenders/list-pretenders.component';
import { LikeComponent } from './components/like/like.component';
import { FooterComponent } from './components/footer/footer.component';
import {ZinderService} from './service/zinderService';
import { StatistiqueComponent } from './components/statistique/statistique.component';
import { MatchComponent } from './components/match/match.component';
import { ListMatchComponent } from './components/list-match/list-match.component';
import { DiagramComponent } from './components/diagram/diagram.component';
import { InteretComponent } from './components/interet/interet.component';
import {FormsModule} from '@angular/forms';

const appRoutes: Routes = [
  { path: 'pretenders', component: ListPretendersComponent },
  { path: 'stats', component: StatistiqueComponent },
  { path: '',
    redirectTo: 'pretenders',
    pathMatch: 'full'
  },
  { path: '**', component: ListPretendersComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PretenderComponent,
    ListPretendersComponent,
    LikeComponent,
    FooterComponent,
    StatistiqueComponent,
    MatchComponent,
    ListMatchComponent,
    DiagramComponent,
    InteretComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
  ],
  providers: [
    ZinderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
